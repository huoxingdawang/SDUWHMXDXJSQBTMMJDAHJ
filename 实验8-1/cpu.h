#ifndef __CPU_H
#define __CPU_H
#include <iostream>
using namespace std;
enum CPU_Rank
{
    i3 = 0,
    i5 = 1,
    i7 = 2,
};
class CPU
{
    friend class COMPUTER;
    private:
        CPU_Rank rank;
        int      frequency;
        float    voltage;
        int      price;
    public:
        CPU                     ();
        CPU                     (const CPU_Rank r,const int f,const float v,const int p);
        CPU                     (const CPU &cpu);
        ~CPU                    ();
        CPU_Rank GetRank        ()const;
        int      GetFrequency   ()const;
        float    GetVoltage     ()const;
        void     SetRank        (CPU_Rank r);
        void     SetFrequency   (int f);
        void     SetVoltage     (float v);
        void     Print          ()const;
        void     Run            ()const;
        void     Stop           ()const;
};
#endif