#ifndef __ARRAYOFCPU_H
#define __ARRAYOFCPU_H
#include "cpu.h"
using namespace std;
class ArrayOfCpu
{
    private:
        CPU *cpus;
        int size;
    public:
        ArrayOfCpu();
        ArrayOfCpu(const int size);
        ArrayOfCpu(const ArrayOfCpu &in);
        ~ArrayOfCpu();
        CPU& operator[](const int i);

};
#endif

