#include "ArrayOfCpu.h"
ArrayOfCpu::ArrayOfCpu()
{
    size=0;
    cpus=NULL;
}
ArrayOfCpu::ArrayOfCpu(const int size):size(size)
{
    cpus=new CPU[size];
}
ArrayOfCpu::ArrayOfCpu(const ArrayOfCpu &in):size(in.size)
{
    cpus=new CPU[size];
    for(int i=0;i<size;++i)
        cpus[i]=in.cpus[i];
}
ArrayOfCpu::~ArrayOfCpu()
{
    delete[] cpus;
    size=0;
    cpus=NULL;
}
CPU& ArrayOfCpu:: operator[](const int i)
{
    return cpus[i]; 
}