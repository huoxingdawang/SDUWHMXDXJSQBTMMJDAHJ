#include <iostream>
#include "cpu.h"
#include "ram.h"
#include "computer.h"
#include "ArrayOfCpu.h"
using namespace std;

int main()
{
	CPU* cpus=new CPU[2];
    cpus[0].SetRank(i5);
    cpus[1].SetRank(i7);
    cpus[0].Print();
    cpus[1].Print();
    delete[] cpus;
    
    ArrayOfCpu cpuArray1(2);
    cpuArray1[0].SetRank(i5);
    cpuArray1[1].SetRank(i7);
    cpuArray1[0].Print();
    cpuArray1[1].Print();
    
    ArrayOfCpu cpuArray2=cpuArray1;
    cpuArray2[0].Print();
    cpuArray2[1].Print();
    cpuArray1[0].SetRank(i3);
    cpuArray1[1].SetRank(i5);
    cpuArray1[0].Print();
    cpuArray1[1].Print();    
    cpuArray2[0].Print();
    cpuArray2[1].Print();    
}
