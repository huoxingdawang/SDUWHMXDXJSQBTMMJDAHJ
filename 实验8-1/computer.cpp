#include "computer.h"
int COMPUTER::cnt=0; 
COMPUTER::COMPUTER()
{
    cout<<"默认构造了一个COMPUTER!"<<endl;
    cnt++;
    price=cpu.price+ram.price;
}
COMPUTER::COMPUTER(const CPU c,const RAM r,const int p):cpu(c),ram(r),price(p)
{
    cout<<"初始化构造了一个COMPUTER!"<<endl;
    cnt++;
    price=cpu.price+ram.price;
}
COMPUTER::COMPUTER(const COMPUTER &com):cpu(com.cpu),ram(com.ram),price(com.price)
{
    cout<<"调用COMPUTER的复制构造函数"<<endl;
    cnt++;
    price=cpu.price+ram.price;
}
COMPUTER::~COMPUTER()
{
    cout<<"正在删除COMPUTER对象!"<<endl;
    cnt--;
}
void COMPUTER::SetComputer(CPU c)
{
    cpu=c;
}
void COMPUTER::SetComputer(RAM r)
{
    ram=r;
}
void COMPUTER::SetComputer(int p)
{
    price=p;
}
void COMPUTER::Setprice(int pc,int pr)
{
    cpu.price=pc;
    ram.price=pr;
    price=cpu.price+ram.price;
}
void COMPUTER::Print()const
{
    cout<<"价格:"<<price<<endl;
    cpu.Print();
    ram.Print();
}
void COMPUTER::Run()const
{
    cpu.Run();
    ram.Run();
    cout << "COMPUTER开始运行!" << endl;
}
void COMPUTER::Stop()const
{
    cout << "COMPUTER停止运行!" << endl;
    ram.Stop();
    cpu.Stop();
}