#ifndef __RAM_H
#define __RAM_H
#include <iostream>
using namespace std;
enum RAM_Rank
{
    DDR = 0,
    DDR2= 1,
    DDR3= 2,
};
class RAM
{
    friend class COMPUTER;
    private:
        RAM_Rank rank;
        int      memory;
        float    frequency;
        int      price;
    public:
        RAM                     ();
        RAM                     (const RAM_Rank r,const float f,const int m,const int p);
        RAM                     (const RAM &ram);
        ~RAM                    ();
        RAM_Rank GetRank        () const;
        float    GetFrequency   () const;
        int      GetMemory      () const;
        void     SetRank        (RAM_Rank r);
        void     SetFrequency   (float f);
        void     SetMemory      (int m);
        void     Print          () const;
        void     Run            () const;
        void     Stop           () const;
};
#endif