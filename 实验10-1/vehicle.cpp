#include "vehicle.h"
vehicle::vehicle():MaxSpeed(0),Weight(0),price(0)
{
    cout<<"The vehicle has been constructed"<<endl;
}
vehicle::vehicle(int s, float w, float p):MaxSpeed(s),Weight(w), price(p)
{
    cout<<"The vehicle has been constructed"<<endl;
}
vehicle::vehicle(vehicle &v):MaxSpeed(v.MaxSpeed),Weight(v.Weight)
{
    cout<<"The vehicle has been constructed"<<endl;
}
vehicle::~vehicle()
{
    cout<<"The vehicle has been deconstructed"<<endl;
}
void vehicle::Print()
{
    cout<<MaxSpeed<<","<<Weight<<endl;
}
void vehicle::Run()
{
    cout<<"The vehicle is running!"<<endl;
}
void vehicle::Stop()
{
    cout<<"The vehicle is stopped!"<<endl;
}
