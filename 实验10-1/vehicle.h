#ifndef __VEHICLE_H
#define __VEHICLE_H
#include <iostream>
using namespace std;
class vehicle
{
    private:
        int MaxSpeed;
        float Weight;
        float price;
    public:
        vehicle();
        vehicle(int s,float w,float p);
        vehicle(vehicle &v);
        ~vehicle();
        void Print();
        void Run();
        void Stop();
};
#endif
