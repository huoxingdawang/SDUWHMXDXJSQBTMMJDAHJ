#include "motorcycle.h"
motorcycle::motorcycle():motorcar(),bicycle(),Power(0)
{
    cout<<"The motorcycle has been constructed"<<endl;
}
motorcycle::motorcycle(int s, float w, float price, float l,float h,float g,int p,string m):vehicle(s,w,price),motorcar(s,w,price,l,h,g),bicycle(s,w,price,m),Power(p)
{
    cout<<"The motorcycle has been constructed"<<endl;
}
motorcycle::motorcycle(motorcycle &v):vehicle(v),motorcar(v),bicycle(v),Power(v.Power)
{
    cout<<"The motorcycle has been constructed"<<endl;
}
motorcycle::~motorcycle()
{
    cout<<"The motorcycle has been deconstructed"<<endl;
}
void motorcycle::Print()
{
    motorcar::Print();
    cout<<Power<<endl;
}
void motorcycle::Run()
{
    cout<<"The motorcycle is running!"<<endl;
}
void motorcycle::Stop()
{
    cout<<"The motorcycle is stopped!"<<endl;
}
