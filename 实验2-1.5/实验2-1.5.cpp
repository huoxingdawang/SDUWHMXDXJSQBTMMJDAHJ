#include <iostream>
using namespace std;
enum CPU_Rank
{
    i3,
    i5 = 3,
    i7,
};
istream& operator>> (istream& in, CPU_Rank& type) {
    int a;
    in >> a;
    type = (CPU_Rank)a;
    return in;
}
int main()
{
    CPU_Rank cpu = i5;
    cin >> cpu;
    cout << cpu;
}
