#ifndef __COMPUTER_H
#define __COMPUTER_H
#include "ram.h"
#include "cpu.h"
using namespace std;
class COMPUTER
{
    private:
        CPU cpu;
        RAM ram;
        int price;
        static int cnt;
    public:
        COMPUTER            ();
        COMPUTER            (CPU c,RAM r,int p);
        COMPUTER            (COMPUTER &com);
        ~COMPUTER           ();
        void     SetComputer(CPU c);
        void     SetComputer(RAM r);
        void     SetComputer(int p);
        void     Setprice   (int pc,int pr);
        void     Print      ();
        void     Run        ();
        void     Stop       ();
        static void     Showcount  (){cout<<"��"<<cnt<<"̨����"<<endl;}
};
#endif