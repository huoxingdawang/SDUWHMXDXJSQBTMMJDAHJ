#include <iostream>
#include "cpu.h"
#include "ram.h"
#include "computer.h"
using namespace std;

int main()
{
    CPU      mycpu(i5,2400,2.8,1000);
    RAM      myram(DDR3,8,2.4,1000);
    COMPUTER computer1(mycpu,myram,11000);
    COMPUTER::Showcount();
    computer1.Print();
    computer1.Run  ();
    computer1.Stop ();
    
    computer1.Setprice(10000,20000);
    computer1.Print();
    
    COMPUTER computer2=computer1;
    COMPUTER::Showcount();
    computer2.Print();
    CPU      mycpu2(i3,3200,1.8,200);
    RAM      myram2(DDR2,16,1.4,200);
    computer2.SetComputer(2);
    computer2.SetComputer(mycpu2);
    computer2.SetComputer(myram2);
    computer2.Print();
    COMPUTER::Showcount();
}
