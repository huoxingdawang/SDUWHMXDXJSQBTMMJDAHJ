#include "cpu.h"
CPU::CPU()
{
//  cout<<"正在默认构造对象"<<endl;
    rank=i3;
    frequency=0;
    voltage=0;
    price=0;
}
CPU::CPU(CPU_Rank r,int f,float v,int p):rank(r),frequency(f),voltage(v),price(p)
{
//  cout<<"正在构造对象"<<endl;
}
CPU::CPU(CPU &cpu):rank(cpu.rank),frequency(cpu.frequency),voltage(cpu.voltage),price(cpu.price)
{
//  cout<<"正在复制构造对象"<<endl;
}
CPU::~CPU()
{
//  cout<<"正在删除对象"<<endl;
}
CPU_Rank CPU::GetRank()
{
    return rank;
}
int CPU::GetFrequency()
{
    return frequency;
}
float CPU::GetVoltage()
{
    return voltage;
}
void CPU::SetRank(CPU_Rank r)
{
    rank = r;
}
void CPU::SetFrequency(int f)
{
    frequency = f;
}
void CPU::SetVoltage(float v)
{
    voltage = v;
}
void CPU::Print()
{
    static const char name[][3]={"i3","i5","i7"};
    cout<<"型号:"<<GetRank()<<"("<<name[rank]<<")"<<endl;
    cout<<"电压:"<<GetVoltage()<<endl;
    cout<<"频率:"<<GetFrequency()<<endl;
    cout<<"价格:"<<price<<endl;
}
void CPU::Run()
{
    cout << "CPU开始运行!" << endl;
}
void CPU::Stop()
{
    cout << "CPU停止运行!" << endl;
}