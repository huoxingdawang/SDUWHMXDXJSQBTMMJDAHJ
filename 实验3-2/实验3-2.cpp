#include <iostream>
using namespace std;
enum CPU_Rank
{
    i3=0,
    i5=1,
    i7=2,
};
class CPU
{
    public:
        CPU_Rank GetRank        ();
        int      GetFrequency   ();
        float    GetVoltage     ();
        void     SetRank        (CPU_Rank r);
        void     SetFrequency   (int f);
        void     SetVoltage     (float v);
        void     Print           ();
        void     Run            ();
        void     Stop           ();
    private:
        CPU_Rank rank;
        int      frequency;
        float    voltage;
};
CPU_Rank CPU::GetRank()
{
    return rank;
}
int CPU::GetFrequency()
{
    return frequency;
}
float CPU::GetVoltage()
{
    return voltage;
}
void CPU::SetRank(CPU_Rank r)
{
    rank=r;
}
void CPU::SetFrequency(int f)
{
    frequency=f;
}
void CPU::SetVoltage(float v)
{
    voltage=v;
}
void CPU::Print()
{
    static const char name[][3]={"i3","i5","i7"};
    cout<<"型号:"<<rank<<"("<<name[rank]<<")"<<endl;
    cout<<"电压:"<<voltage<<endl;
    cout<<"频率:"<<frequency<<endl;
}
void CPU::Run()
{
    cout << "CPU开始运行!" << endl;
}
void CPU::Stop()
{
    cout << "CPU停止运行!" << endl;
}
int main()
{
    CPU cup;
    cup.SetRank(i7);
    cup.SetFrequency(1024);
    cup.SetVoltage(0.1);
    cout<<"型号:"<<cup.GetRank()<<endl;
    cout<<"电压:"<<cup.GetVoltage()<<endl;
    cout<<"频率:"<<cup.GetFrequency()<<endl;
    cup.Print();
    cup.Run();
    cup.Stop();
}
