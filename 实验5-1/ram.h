#ifndef __RAM_H
#define __RAM_H
#include <iostream>
using namespace std;
enum RAM_Rank
{
    DDR = 0,
    DDR2= 1,
    DDR3= 2,
};
class RAM
{
    private:
        RAM_Rank rank;
        int      memory;
        float    frequency;
    public:
        RAM                     ();
        RAM                     (RAM_Rank r,float f,int m);
        RAM                     (RAM &ram);
        ~RAM                    ();
        RAM_Rank GetRank        ();
        float    GetFrequency   ();
        int      GetMemory      ();
        void     SetRank        (RAM_Rank r);
        void     SetFrequency   (float f);
        void     SetMemory      (int m);
        void     Print          ();
        void     Run            ();
        void     Stop           ();
};
#endif