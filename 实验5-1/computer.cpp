#include "computer.h"
COMPUTER::COMPUTER()
{
    cout<<"默认构造了一个COMPUTER!"<<endl;
}
COMPUTER::COMPUTER(CPU c,RAM r,int p):cpu(c),ram(r),price(p)
{
    cout<<"初始化构造了一个COMPUTER!"<<endl;
}
COMPUTER::COMPUTER(COMPUTER &com):cpu(com.cpu),ram(com.ram),price(com.price)
{
    cout<<"调用COMPUTER的复制构造函数"<<endl;
}
COMPUTER::~COMPUTER()
{
    cout<<"正在删除COMPUTER对象!"<<endl;
}
void COMPUTER::SetComputer(CPU c)
{
    cpu=c;
}
void COMPUTER::SetComputer(RAM r)
{
    ram=r;
}
void COMPUTER::SetComputer(int p)
{
    price=p;
}
void COMPUTER::Print()
{
    cpu.Print();
    ram.Print();
    cout<<"价格:"<<price<<endl;
}
void COMPUTER::Run()
{
    cpu.Run();
    ram.Run();
    cout << "COMPUTER开始运行!" << endl;
}
void COMPUTER::Stop()
{
    cout << "COMPUTER停止运行!" << endl;
    ram.Stop();
    cpu.Stop();
}