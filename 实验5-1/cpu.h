#ifndef __CPU_H
#define __CPU_H
#include <iostream>
using namespace std;
enum CPU_Rank
{
    i3 = 0,
    i5 = 1,
    i7 = 2,
};
class CPU
{
    public:
        CPU                     ();
        CPU                     (CPU_Rank r,int f,float v);
        CPU                     (CPU &cpu);
        ~CPU                    ();
        CPU_Rank GetRank        ();
        int      GetFrequency   ();
        float    GetVoltage     ();
        void     SetRank        (CPU_Rank r);
        void     SetFrequency   (int f);
        void     SetVoltage     (float v);
        void     Print          ();
        void     Run            ();
        void     Stop           ();
    private:
        CPU_Rank rank;
        int      frequency;
        float    voltage;
};
#endif