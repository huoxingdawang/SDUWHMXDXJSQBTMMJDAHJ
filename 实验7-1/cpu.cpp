#include "cpu.h"
CPU::CPU()
{
//  cout<<"正在默认构造对象"<<endl;
    rank=i3;
    frequency=0;
    voltage=0;
    price=0;
}
CPU::CPU(const CPU_Rank r,const int f,const float v,const int p):rank(r),frequency(f),voltage(v),price(p)
{
//  cout<<"正在构造对象"<<endl;
}
CPU::CPU(const CPU &cpu):rank(cpu.rank),frequency(cpu.frequency),voltage(cpu.voltage),price(cpu.price)
{
//  cout<<"正在复制构造对象"<<endl;
}
CPU::~CPU()
{
//  cout<<"正在删除对象"<<endl;
}
CPU_Rank CPU::GetRank()const
{
    return rank;
}
int CPU::GetFrequency()const
{
    return frequency;
}
float CPU::GetVoltage()const
{
    return voltage;
}
void CPU::SetRank(CPU_Rank r)
{
    rank = r;
}
void CPU::SetFrequency(int f)
{
    frequency = f;
}
void CPU::SetVoltage(float v)
{
    voltage = v;
}
void CPU::Print()const
{
    static const char name[][3]={"i3","i5","i7"};
    cout<<"型号:"<<GetRank()<<"("<<name[rank]<<")"<<endl;
    cout<<"电压:"<<GetVoltage()<<endl;
    cout<<"频率:"<<GetFrequency()<<endl;
    cout<<"价格:"<<price<<endl;
}
void CPU::Run()const
{
    cout << "CPU开始运行!" << endl;
}
void CPU::Stop()const
{
    cout << "CPU停止运行!" << endl;
}