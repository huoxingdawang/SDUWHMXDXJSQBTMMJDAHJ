#ifndef __COMPUTER_H
#define __COMPUTER_H
#include "ram.h"
#include "cpu.h"
using namespace std;
class COMPUTER
{
    private:
        CPU cpu;
        RAM ram;
        int price;
        static int cnt;
    public:
        COMPUTER            ();
        COMPUTER            (const CPU c,const RAM r,const int p);
        COMPUTER            (const COMPUTER &com);
        ~COMPUTER           ();
        void     SetComputer(CPU c);
        void     SetComputer(RAM r);
        void     SetComputer(int p);
        void     Setprice   (int pc,int pr);
        void     Print      ()const;
        void     Run        ()const;
        void     Stop       ()const;
        static void     Showcount  (){cout<<"��"<<cnt<<"̨����"<<endl;}
};
#endif