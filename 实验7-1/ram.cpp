#include "ram.h"
RAM::RAM()
{
//  cout<<"正在默认构造对象"<<endl;
    rank=DDR;
    frequency=0;
    memory=0;
    price=0;
}
RAM::RAM(const RAM_Rank r,const float f,const int m,const int p):rank(r),frequency(f),memory(m),price(p)
{
//  cout<<"正在构造对象"<<endl;
}
RAM::RAM(const RAM &ram):rank(ram.rank),frequency(ram.frequency),memory(ram.memory),price(ram.price)
{
//  cout<<"正在复制构造对象"<<endl;
}
RAM::~RAM()
{
//  cout<<"正在删除对象"<<endl;
}
RAM_Rank RAM::GetRank() const
{
    return rank;
}
float RAM::GetFrequency() const
{
    return frequency;
}
int RAM::GetMemory() const
{
    return memory;
}
void RAM::SetRank(RAM_Rank r)
{
    rank=r;
}
void RAM::SetFrequency(float f)
{
    frequency=f;
}
void RAM::SetMemory(int m)
{
    memory=m;
}
void RAM::Print() const
{
    static const char name[][5]={"DDR","DDR2","DDR3"};
    cout<<"型号:"<<GetRank()<<"("<<name[rank]<<")"<<endl;
    cout<<"频率:"<<GetFrequency()<<endl;
    cout<<"大小:"<<GetMemory()<<endl;
    cout<<"价格:"<<price<<endl;
}
void RAM::Run() const
{
    cout << "RAM开始运行!" << endl;
}
void RAM::Stop() const
{
    cout << "RAM停止运行!" << endl;
}