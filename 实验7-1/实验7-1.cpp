#include <iostream>
#include "cpu.h"
#include "ram.h"
#include "computer.h"
using namespace std;

int main()
{
    const CPU      mycpu(i5,2400,2.8,1000);
    const RAM      myram(DDR3,8,2.4,1000);
    const COMPUTER computer1(mycpu,myram,11000);
    COMPUTER::Showcount();
    computer1.Print();
    computer1.Run  ();
    computer1.Stop ();

//不能正常过编译
//    computer1.Setprice(10000,20000);
    computer1.Print();
    
    const COMPUTER computer2=computer1;
    COMPUTER::Showcount();
    computer2.Print();
//不能正常过编译
//  computer2.Setprice(10000,20000);
}
