#ifndef __MOTORCAR_H
#define __MOTORCAR_H
#include <iostream>
#include "vehicle.h"
using namespace std;
class motorcar :
    virtual public vehicle
{
    private:
        float Length;
        float Height;
        float Gas;
    public:
        motorcar();
        motorcar(int s,float w,float p,float l,float h,float g);
        motorcar(motorcar &v);
        ~motorcar();
        void Print();
        void Run();
        void Stop();
};

#endif
