#include "bicycle.h"
bicycle::bicycle():vehicle(),material()
{
    cout<<"The bicycle has been constructed by default constructor"<<endl;
}
bicycle::bicycle(int s, float w, float pri,string m):vehicle(s,w,pri),material(m)
{
    cout<<"The bicycle has been constructed"<<endl;
}
bicycle::bicycle(bicycle &v):vehicle(v),material(v.material)
{
}
bicycle::~bicycle()
{
    cout<<"The bicycle has been deconstructed"<<endl;
}
void bicycle::Print()
{
    vehicle::Print();
    cout<<material<<endl;
}
void bicycle::Run()
{
    cout<<"The bicycle is running!"<<endl;
}
void bicycle::Stop()
{
    cout<<"The bicycle is stopped!"<<endl;
}
