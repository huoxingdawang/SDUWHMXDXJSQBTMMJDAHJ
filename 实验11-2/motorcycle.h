#ifndef __MOTORCYCLE_H
#define __MOTORCYCLE_H
#include <iostream>
#include "motorcar.h"
#include "bicycle.h"
using namespace std;
class motorcycle :
    public motorcar,
    public bicycle
{
    private:
        int Power;
    public:
        motorcycle();
        motorcycle(int s,float w,float price,float l,float h,float g,int p,string m);
        motorcycle(motorcycle &v);
        ~motorcycle();
        void Print();
        void Run();
        void Stop();
};

#endif
