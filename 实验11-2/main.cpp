#include "vehicle.h"
#include "motorcar.h"
#include "motorcycle.h"
void fun(vehicle * ptr)
{
    ptr->Print();
    ptr->Run();
    ptr->Stop();
}
int main()
{
    vehicle *p=NULL;
    motorcycle  mo;
    vehicle     ve;
    motorcar    mc;
    bicycle     bi;
    
    p=&mo;fun(p);
    p=&ve;fun(p);
    p=&mc;fun(p);
    p=&bi;fun(p);
    
    return 0;
}