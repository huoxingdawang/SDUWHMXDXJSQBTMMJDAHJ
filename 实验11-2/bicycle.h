#ifndef __BICYCLE_H
#define __BICYCLE_H
#include "vehicle.h"
#include "string"
class bicycle :
    virtual public vehicle
{
    private:
        string material;
    public:
        bicycle();
        bicycle(int s, float w, float pri,string m);
        bicycle(bicycle &v);
        ~bicycle();
        void Print();
        void Run();
        void Stop();    
};

#endif
