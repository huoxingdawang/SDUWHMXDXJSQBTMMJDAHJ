#include <iostream>
using namespace std;
enum CPU_Rank
{
    i3 = 0,
    i5 = 1,
    i7 = 2,
};
class CPU
{
    public:
        CPU();
        CPU(CPU_Rank r,int f,float v);
        CPU(CPU &cpu);
        ~CPU();
        CPU_Rank GetRank();
        int      GetFrequency();
        float    GetVoltage();
        void     SetRank(CPU_Rank r);
        void     SetFrequency(int f);
        void     SetVoltage(float v);
        void     Show();
        void     Run();
        void     Stop();
    private:
        CPU_Rank rank;
        int      frequency;
        float    voltage;
};
CPU::CPU()
{
    cout<<"正在默认构造对象"<<endl;
    rank=i3;
    frequency=0;
    voltage=0;
}
CPU::CPU(CPU_Rank r,int f,float v):rank(r),frequency(f),voltage(v)
{
    cout<<"正在构造对象"<<endl;
}
CPU::CPU(CPU &cpu):rank(cpu.rank),frequency(cpu.frequency),voltage(cpu.voltage)
{
    cout<<"正在复制构造对象"<<endl;
}
CPU::~CPU()
{
    cout<<"正在删除对象"<<endl;
}
CPU_Rank CPU::GetRank()
{
    return rank;
}
int CPU::GetFrequency()
{
    return frequency;
}
float CPU::GetVoltage()
{
    return voltage;
}
void CPU::SetRank(CPU_Rank r)
{
    rank = r;
}
void CPU::SetFrequency(int f)
{
    frequency = f;
}
void CPU::SetVoltage(float v)
{
    voltage = v;
}
void CPU::Show()
{
    static const char name[][3]={"i3","i5","i7"};
    cout<<"型号:"<<rank<<"("<<name[rank]<<")"<<endl;
    cout<<"电压:"<<voltage<<endl;
    cout<<"频率:"<<frequency<<endl;
}
void CPU::Run()
{
    cout << "CPU开始运行!" << endl;
}
void CPU::Stop()
{
    cout << "CPU停止运行!" << endl;
}
void f1(CPU cpu)
{
}
CPU f2()
{
    CPU cpu(i5,0,1000);
    return cpu;
}

int main()
{
    CPU cup0,cup1(i7,1000,1000);
    cup0.Show();
    cup1.Show();
    CPU cup2=cup1;
    f1(cup2);
    cup2=f2();
}
