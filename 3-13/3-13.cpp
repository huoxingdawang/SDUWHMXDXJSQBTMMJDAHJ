#include <iostream>
using namespace std;
typedef unsigned long long ULL;
#define MAXN 10000011
ULL cache[MAXN];
ULL fibonacci(unsigned long long n)
{
    if (n == 1 || n == 2)return 1;
    if (cache[n])return cache[n];
    cache[n] = fibonacci(n - 1) + fibonacci(n - 2);
    cout << "F(" << n << ")=" << cache[n] << endl;
    return cache[n];
}
int main()
{
    fibonacci(100);
}

