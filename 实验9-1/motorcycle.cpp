#include "motorcycle.h"
motorcycle::motorcycle():motorcar(0,0,0,0,0),Power(0)
{
    cout<<"The motorcycle has been constructed"<<endl;
}
motorcycle::motorcycle(int s,float w,float l,float h,float g,int p):motorcar(s,w,l,h,g),Power(p)
{
    cout<<"The motorcycle has been constructed"<<endl;
}
motorcycle::motorcycle(motorcycle &v):motorcar(v),Power(v.Power)
{
    cout<<"The motorcycle has been constructed"<<endl;
}
motorcycle::~motorcycle()
{
    cout<<"The motorcycle has been deconstructed"<<endl;
}
void motorcycle::Print()
{
    motorcar::Print();
    cout<<Power<<endl;
}
void motorcycle::Run()
{
    cout<<"The motorcycle is running!"<<endl;
}
void motorcycle::Stop()
{
    cout<<"The motorcycle is stopped!"<<endl;
}
