#ifndef __MOTORCYCLE_H
#define __MOTORCYCLE_H
#include <iostream>
#include "motorcar.h"
using namespace std;
class motorcycle :
    public motorcar
{
    private:
        int Power;
    public:
        motorcycle();
        motorcycle(int s,float w,float l,float h,float g,int p);
        motorcycle(motorcycle &v);
        ~motorcycle();
        void Print();
        void Run();
        void Stop();
};

#endif
