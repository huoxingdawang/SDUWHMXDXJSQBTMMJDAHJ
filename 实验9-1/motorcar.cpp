#include "motorcar.h"
motorcar::motorcar():vehicle(0,0),Length(0),Height(0),Gas(0)
{
    cout<<"The motorcar has been constructed"<<endl;
}
motorcar::motorcar(int s,float w,float l,float h,float g):vehicle(s,w),Length(l),Height(h),Gas(g)
{
    cout<<"The motorcar has been constructed"<<endl;
}
motorcar::motorcar(motorcar &v):vehicle(v),Length(v.Length),Height(v.Height),Gas(v.Gas)
{
    cout<<"The motorcar has been constructed"<<endl;
}
motorcar::~motorcar()
{
    cout<<"The motorcar has been deconstructed"<<endl;
}
void motorcar::Print()
{
    vehicle::Print();
    cout<<Length<<","<<Height<<","<<Gas<<endl;
}
void motorcar::Run()
{
    cout<<"The motorcar is running!"<<endl;
}
void motorcar::Stop()
{
    cout<<"The motorcar is stopped!"<<endl;
}
