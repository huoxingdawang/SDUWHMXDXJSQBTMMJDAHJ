#include <iostream>
#include <string>
using namespace std;
void change1(string s)
{
    s = "alter";
}
void change2(string& s)
{
    s = "alter";
}

int main()
{
    string str = "original";
    cout << str << endl;
    change1(str);
    cout << str << endl;
    change2(str);
    cout << str << endl;
}