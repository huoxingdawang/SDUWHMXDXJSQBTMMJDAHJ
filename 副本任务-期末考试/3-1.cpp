#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <cstdlib>
#include <map>
#include <ctime>
#include <vector>
#include <queue>
using namespace std;
namespace aaa
{
    class A{public:virtual void fun(void) const {cout<<"A"<<endl;};};
    class B{public:virtual void fun(void) const {cout<<"B"<<endl;};};
    class C:public A,public B{public:virtual void fun(void) const {cout<<"C"<<endl;};};
    void test()
    {
        C c;
        A &pa=c;
        B &pb=c;
        C &pc=c;
        pa.fun();
        pb.fun();
        pc.fun();
    }
};
namespace bbb
{
    class A{public:void fun(void) const {cout<<"A"<<endl;};};
    class B{public:void fun(void) const {cout<<"B"<<endl;};};
    class C:public A,public B{public:void fun(void) const {cout<<"C"<<endl;};};
    void test()
    {
        C c;
        A &pa=c;
        B &pb=c;
        C &pc=c;
        pa.fun();
        pb.fun();
        pc.fun();
    }
};

int main()
{
    aaa::test();
    bbb::test();
    return 0;
}