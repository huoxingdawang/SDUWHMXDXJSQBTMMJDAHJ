#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <cstdlib>
#include <map>
#include <ctime>
#include <vector>
#include <queue>
using namespace std;
class test
{
    private:
        int a[10];
    public:
        test()
        {
            for(int i=0;i<10;++i)
                a[i]=i*10;
        }
        int turn()
        {
            int temp=a[0];
            for(int i=0;i<10;++i)
                if(temp<a[i])
                    temp=a[i];
            return temp;
        }
};
int main()
{
    test t;
    cout<<t.turn();
    return 0;
}