#include <iostream>
using namespace std;
class Point
{
    private:
        int x,y;
    public:
        Point();
        Point(const int xx,const int yy);
        Point(const Point &p);
        ~Point();
        Point   operator+   (const Point& p) const;
        Point   operator-   (const Point& p) const;
        Point&  operator++  ();
        Point   operator++  (int);
        Point&  operator--  ();
        Point   operator--  (int);
        friend ostream & operator<<(ostream &out,const Point &a);
};
Point::Point():x(0),y(0){}
Point::Point(const int xx,const int yy):x(xx),y(yy){}
Point::Point(const Point &p):x(p.x),y(p.y){}
Point::~Point(){}
ostream & operator<<(ostream &out,const Point &a)
{
    out<<a.x<<","<<a.y;
    return out;
}
Point Point::operator+(const Point& p) const
{
    return Point(x+p.x,y+p.y);
}
Point Point::operator-(const Point& p) const
{
    return Point(x-p.x,y-p.y);
}
Point& Point::operator++()
{
    ++x;
    ++y;
    return *this;
}
Point Point::operator++(int)
{
    Point old=*this;
    ++x;
    ++y;
    return old;
}
Point& Point::operator--()
{
    --x;
    --y;
    return *this;
}
Point Point::operator--(int)
{
    Point old=*this;
    --x;
    --y;
    return old;
}
int main()
{
    Point p1(1,1),p2(2,2);
    cout<<(p1+p2)<<endl;
    cout<<(p1-p2)<<endl;
    cout<<(p1++)<<endl;
    cout<<(++p1)<<endl;
    cout<<(p2--)<<endl;
    cout<<(--p2)<<endl;

    return 0;
}
