#include <iostream>
#include <cassert>
#include <cstdio>
#include <cstdlib>
using namespace std;
template<class T>
class Array
{
private:
    T* list;
    int size;
public:
    Array(int s=0)
    {
        assert(s >= 0);
        size = s;
        list = new T[size]();
    }
    Array(const Array<T> &a):size(a.size)
    {
        list=new T[size];
        for(int i=0;i<size;++i)
            list[i]=a.list[i];
    }
    ~Array()
    {
        size=0;
        delete[] list;
    }
    friend ostream & operator<<(ostream &out,const Array<T> &a)
    {
        out<<a.size<< ":";
        for (int i = 0; i < a.size; ++i)
            out << a.list[i] << ",";
        return out;
    }
    T & operator[](int i)
    {
        assert(i>=0&&i<=size);
        return list[i];
    }
    const T & operator[](int i) const
    {
        assert(i>=0&&i<=size);
        return list[i];
    }
    Array<T>& operator=(const Array<T> & that)
    {
        if(&that!=this)
        {
            delete[] list;
            size=that.size;
            list = new T[size]();
            for(int i=0;i<size;++i)
                list[i]=that.list[i];
        }
        return *this;
    }
    Array<T> operator+(const Array<T> & that)
    {
        if(size!=that.size)
            return Array(0);
        Array res(size);
        for(int i=0;i<size;++i)
            res.list[i]=list[i]+that.list[i];
        return res;
    }
    Array<T> operator-(const Array<T> & that)
    {
        if(size!=that.size)
            return Array(0);
        Array res(size);
        for(int i=0;i<size;++i)
            res.list[i]=list[i]-that.list[i];
        return res;
    }
    operator T *()
    {
        return list;
    }
    operator const T *() const
    {
        return list;
    }
};
int main()
{
    Array<int> a(10);
    Array<int> b(10);
    a[0]=1;
    a[2]=100;
    cout<<a<<endl;
    cout<<b<<endl;
    b=a;
    cout<<b<<endl;
    
    b[1]=1000;
    cout<<a<<"+"<<b<<"="<<(a+b)<<endl;
    cout<<a<<"-"<<b<<"="<<(a-b)<<endl;
    
    cout<<"size not same"<<(Array<int>(10)-Array<int>(9))<<endl;
    
    cout<<(*(Array<int>(10)))<<endl;
    cout<<(*b)<<endl;
    
    return 0;
}